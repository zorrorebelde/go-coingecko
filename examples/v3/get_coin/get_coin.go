package main

import (
	"fmt"

	coingecko "gitlab.com/zorrorebelde/go-coingecko/v3"
)

func main() {
	client := coingecko.NewClient()
	params := coingecko.GetCoinParams{}
	coin, err := client.GetCoin("bitcoin", params)

	if err == nil {
		fmt.Println(coin)
	} else {
		fmt.Println("Failed to get coin", err)
	}
}
