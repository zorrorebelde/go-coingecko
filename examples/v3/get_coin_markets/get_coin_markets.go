package main

import (
	"fmt"

	coingecko "gitlab.com/zorrorebelde/go-coingecko/v3"
)

func main() {
	params := coingecko.GetCoinMarketsParams{VSCurrency: "nzd"}
	client := coingecko.NewClient()

	markets, err := client.GetCoinMarkets(params)

	if err == nil {
		fmt.Println(markets)
	} else {
		fmt.Println("Failed to list markets", err)
	}
}
