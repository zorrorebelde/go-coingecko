package main

import (
	"fmt"

	coingecko "gitlab.com/zorrorebelde/go-coingecko/v3"
)

func main() {
	client := coingecko.NewClient()
	coins, err := client.GetCoins()

	if err == nil {
		fmt.Println(coins)
	} else {
		fmt.Println("Failed to list coins", err)
	}
}
