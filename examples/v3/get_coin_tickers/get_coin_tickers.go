package main

import (
	"fmt"

	coingecko "gitlab.com/zorrorebelde/go-coingecko/v3"
)

func main() {
	params := coingecko.GetCoinTickersParams{
		IncludeExchangeLogo: true,
		Order:               "volume_desc",
	}
	client := coingecko.NewClient()

	tickers, err := client.GetCoinTickers("ethereum", params)

	if err == nil {
		fmt.Println(tickers)
	} else {
		fmt.Println("Failed to list coin tickers", err)
	}
}
