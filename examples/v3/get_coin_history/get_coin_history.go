package main

import (
	"fmt"

	coingecko "gitlab.com/zorrorebelde/go-coingecko/v3"
)

func main() {
	params := coingecko.GetCoinHistoryParams{
		Date:         "01-01-2021",
		Localization: true,
	}
	client := coingecko.NewClient()

	history, err := client.GetCoinHistory("cardano", params)

	if err == nil {
		fmt.Println(history)
	} else {
		fmt.Println("Failed to list coin history", err)
	}
}
