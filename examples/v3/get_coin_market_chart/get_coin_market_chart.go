package main

import (
	"fmt"

	coingecko "gitlab.com/zorrorebelde/go-coingecko/v3"
)

func main() {
	params := coingecko.GetCoinMarketChartParams{
		VSCurrency: "nzd",
		Days:       5,
	}
	client := coingecko.NewClient()

	marketChart, err := client.GetCoinMarketChart("stellar", params)

	if err == nil {
		fmt.Println(marketChart)
	} else {
		fmt.Println(err)
	}
}
