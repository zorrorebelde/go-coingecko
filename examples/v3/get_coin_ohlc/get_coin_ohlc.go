package main

import (
	"fmt"

	coingecko "gitlab.com/zorrorebelde/go-coingecko/v3"
)

func main() {
	params := coingecko.GetCoinOHLCParams{
		VSCurrency: "nzd",
		Days:       7,
	}
	client := coingecko.NewClient()

	ohlc, err := client.GetCoinOHLC("ethereum", params)

	if err == nil {
		fmt.Println(ohlc)
	} else {
		fmt.Println(err)
	}
}
