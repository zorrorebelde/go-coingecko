package v3

type Client struct {
	api *API
}

func NewClient(opts ...APIOption) *Client {
	return &Client{
		api: NewAPI(opts...),
	}
}

func (c *Client) GetCoins() ([]ListedCoin, error) {
	var coins []ListedCoin
	err := c.api.get("/coins/list", &coins)
	return coins, err
}

func (c *Client) GetCoinMarkets(params GetCoinMarketsParams) ([]CoinMarket, error) {
	var markets []CoinMarket
	err := c.api.getWithParams("/coins/markets", params, &markets)
	return markets, err
}

func (c *Client) GetCoin(id string, params GetCoinParams) (Coin, error) {
	var coin Coin
	err := c.api.getWithParams("/coins/"+id, params, &coin)
	return coin, err
}

func (c *Client) GetCoinTickers(id string, params GetCoinTickersParams) (CoinTickers, error) {
	var tickers CoinTickers
	err := c.api.getWithParams("/coins/"+id+"/tickers", params, &tickers)
	return tickers, err
}

func (c *Client) GetCoinHistory(id string, params GetCoinHistoryParams) (CoinHistory, error) {
	var history CoinHistory
	err := c.api.getWithParams("/coins/"+id+"/history", params, &history)
	return history, err
}

func (c *Client) GetCoinMarketChart(id string, params GetCoinMarketChartParams) (CoinMarketChart, error) {
	var marketChart CoinMarketChart
	err := c.api.getWithParams("/coins/"+id+"/market_chart", params, &marketChart)
	return marketChart, err
}

func (c *Client) GetCoinMarketChartRange(id string, params GetCoinMarketChartRangeParams) (CoinMarketChart, error) {
	var marketChart CoinMarketChart
	err := c.api.getWithParams("/coins/"+id+"/market_chart/range", params, &marketChart)
	return marketChart, err
}

func (c *Client) GetCoinStatusUpdates(id string, params GetCoinStatusUpdatesParams) (CoinStatusUpdates, error) {
	var statusUpdates CoinStatusUpdates
	err := c.api.getWithParams("/coins/"+id+"/status_updates", params, &statusUpdates)
	return statusUpdates, err
}

func (c *Client) GetCoinOHLC(id string, params GetCoinOHLCParams) (CoinOHLC, error) {
	var ohlc CoinOHLC
	err := c.api.getWithParams("/coins/"+id+"/ohlc", params, &ohlc)
	return ohlc, err
}

func (c *Client) GetCoinsCategoriesList() ([]CoinCategoryList, error) {
	var list []CoinCategoryList
	err := c.api.get("/coins/categories/list", &list)
	return list, err
}
