package v3

import "time"

type APIOptions struct {
	RetryMax     int
	RetryWaitMin time.Duration
	RetryWaitMax time.Duration
	Timeout      time.Duration
	BaseURL      string
}

type APIOption interface {
	apply(o *APIOptions)
}

// RetryMax field
type retryMax int

func (r retryMax) apply(o *APIOptions) {
	o.RetryMax = int(r)
}

func WithRetryMax(max int) APIOption {
	return retryMax(max)
}

// RetryWaitMin field
type retryWaitMin time.Duration

func (r retryWaitMin) apply(o *APIOptions) {
	o.RetryWaitMin = time.Duration(r)
}

func WithRetryWaitMin(min time.Duration) APIOption {
	return retryWaitMin(min)
}

// RetryMax field
type retryWaitMax time.Duration

func (r retryWaitMax) apply(o *APIOptions) {
	o.RetryWaitMax = time.Duration(r)
}

func WithRetryWaitMax(max time.Duration) APIOption {
	return retryWaitMax(max)
}

// RetryMax field
type timeout time.Duration

func (r timeout) apply(o *APIOptions) {
	o.Timeout = time.Duration(r)
}

func WithTimeout(wait time.Duration) APIOption {
	return timeout(wait)
}

// BaseURL field
type baseAPIUrl string

func (b baseAPIUrl) apply(o *APIOptions) {
	o.BaseURL = string(b)
}

func WithBaseURL(u string) APIOption {
	return baseAPIUrl(u)
}
