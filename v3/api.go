package v3

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/hashicorp/go-cleanhttp"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/insprac/qe"
)

const (
	baseURL = "https://api.coingecko.com/api/v3"
)

type API struct {
	client  *http.Client
	baseURL string
}

func NewAPI(opts ...APIOption) *API {
	apiOpts := &APIOptions{
		RetryMax:     100,
		RetryWaitMin: 1 * time.Minute,
		RetryWaitMax: 70 * time.Second,      // 1 minute 10 seconds
		Timeout:      1 * time.Minute * 100, // 1 hour 40 minutes
	}

	for _, o := range opts {
		o.apply(apiOpts)
	}

	// setup retryablehttp client with options
	retryClient := &retryablehttp.Client{
		HTTPClient:   cleanhttp.DefaultPooledClient(),
		RetryWaitMin: apiOpts.RetryWaitMin,
		RetryWaitMax: apiOpts.RetryWaitMax,
		RetryMax:     apiOpts.RetryMax,
		CheckRetry:   retryablehttp.DefaultRetryPolicy,
		Backoff:      retryablehttp.DefaultBackoff, // TODO: wrap this to provide insight
	}

	stdClient := retryClient.StandardClient()
	stdClient.Timeout = apiOpts.Timeout

	if apiOpts.BaseURL == "" {
		apiOpts.BaseURL = baseURL
	}

	return &API{
		client:  stdClient,
		baseURL: apiOpts.BaseURL,
	}
}

func (a *API) get(path string, data interface{}) error {
	resp, err := a.client.Get(baseURL + path)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	bodyBytes, err := io.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		if err == nil {
			return newError("bad response (status=%d, body=%v)",
				resp.StatusCode,
				string(bodyBytes))
		} else {
			return newError("bad response (status=%d)", resp.StatusCode)
		}
	}

	if err != nil {
		return err
	}

	return json.Unmarshal(bodyBytes, &data)
}

func (a *API) getWithParams(path string, params interface{}, data interface{}) error {
	queryString, err := qe.Marshal(params)
	if err != nil {
		return err
	}

	if queryString == "" {
		return a.get(path, data)
	} else {
		return a.get(path+"?"+queryString, data)
	}
}

func newError(message string, args ...interface{}) error {
	return fmt.Errorf(message, args...)
}
