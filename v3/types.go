package v3

import (
	"github.com/shopspring/decimal"
)

type ListedCoin struct {
	ID     string `json:"id"`
	Symbol string `json:"symbol"`
	Name   string `json:"name"`
}

type Coin struct {
	ID                           string                  `json:"id"`
	Symbol                       string                  `json:"symbol"`
	Name                         string                  `json:"name"`
	AssetPlatformID              string                  `json:"asset_platform_id"`
	BlockTimeInMinutes           *decimal.Decimal        `json:"block_time_in_minutes"`
	HashingAlgorithm             string                  `json:"hashing_algorithm"`
	Categories                   []string                `json:"categories"`
	PublicNotice                 string                  `json:"public_notice"`
	AdditionalNotice             string                  `json:"additional_notice"`
	Localization                 map[string]string       `json:"localization"`
	Description                  map[string]string       `json:"description"`
	Links                        CoinLinks               `json:"links"`
	Image                        ImageLinks              `json:"image"`
	CountryOrigin                string                  `json:"country_origin"`
	GenesisDate                  string                  `json:"genesis_date"`
	SentimentVotesUpPercentage   *decimal.Decimal        `json:"sentiment_votes_up_percentage"`
	SentimentVotesDownPercentage *decimal.Decimal        `json:"sentiment_votes_down_percentage"`
	MarketCapRank                *decimal.Decimal        `json:"market_cap_rank"`
	CoinGeckoRank                *decimal.Decimal        `json:"coingecko_rank"`
	CoinGeckoScore               *decimal.Decimal        `json:"coingecko_score"`
	DeveloperScore               *decimal.Decimal        `json:"developer_score"`
	CommunityScore               *decimal.Decimal        `json:"community_score"`
	LiquidityScore               *decimal.Decimal        `json:"liquidity_score"`
	PublicInterestScore          *decimal.Decimal        `json:"public_interest_score"`
	MarketData                   CoinMarketData          `json:"market_data"`
	CommunityData                CoinCommunityData       `json:"community_data"`
	DeveloperData                CoinDeveloperData       `json:"developer_data"`
	PublicInterestStats          CoinPublicInterestStats `json:"public_interest_stats"`
	StatusUpdates                []CoinStatusUpdate      `json:"status_updates"`
	LastUpdated                  string                  `json:"last_updated"`
	Tickers                      []CoinTicker            `json:"tickers"`
}

type CoinLinks struct {
	Homepage                    []string `json:"homepage"`
	BlockchainSite              []string `json:"blockchain_site"`
	OfficialForumURL            []string `json:"official_forum_url"`
	ChatURL                     []string `json:"chat_url"`
	AnnouncementURL             []string `json:"announcement_url"`
	TwitterScreenName           string   `json:"twitter_screen_name"`
	FacebookUsername            string   `json:"facebook_username"`
	BitcointalkThreadIdentifier *int     `json:"bitcointalk_thread_identifier"`
	TelegramChannelIdentifier   string   `json:"telegram_channel_identifier"`
	SubredditURL                string   `json:"subreddit_url"`
	ReposURL                    RepoURLs `json:"repos_url"`
}

type RepoURLs struct {
	GitHub    []string `json:"github"`
	Bitbucket []string `json:"bitbucket"`
}

type ImageLinks struct {
	Thumb string `json:"thumb"`
	Small string `json:"small"`
	Large string `json:"large"`
}

type CoinMarketData struct {
	CurrentPrice                           map[string]*decimal.Decimal `json:"current_price"`
	ROI                                    ReturnOnInvestment          `json:"roi"`
	ATH                                    map[string]*decimal.Decimal `json:"ath"`
	ATHChangePercentage                    map[string]*decimal.Decimal `json:"ath_change_percentage"`
	ATHDate                                map[string]string           `json:"ath_date"`
	ATL                                    map[string]*decimal.Decimal `json:"atl"`
	ATLChangePercentage                    map[string]*decimal.Decimal `json:"atl_change_percentage"`
	ATLDate                                map[string]string           `json:"atl_date"`
	MarketCapRank                          *decimal.Decimal            `json:"market_cap_rank"`
	FullyDilutedValuation                  map[string]*decimal.Decimal `json:"fully_diluted_valuation"`
	High24h                                map[string]*decimal.Decimal `json:"high_24h"`
	Low24h                                 map[string]*decimal.Decimal `json:"low_24h"`
	PriceChange24h                         *decimal.Decimal            `json:"price_change_24h"`
	PriceChangePercentage24h               *decimal.Decimal            `json:"price_change_percentage_24h"`
	PriceChangePercentage7d                *decimal.Decimal            `json:"price_change_percentage_7d"`
	PriceChangePercentage14d               *decimal.Decimal            `json:"price_change_percentage_14d"`
	PriceChangePercentage30d               *decimal.Decimal            `json:"price_change_percentage_30d"`
	PriceChangePercentage60d               *decimal.Decimal            `json:"price_change_percentage_60d"`
	PriceChangePercentage200d              *decimal.Decimal            `json:"price_change_percentage_200d"`
	PriceChangePercentage1y                *decimal.Decimal            `json:"price_change_percentage_1y"`
	MarketCapChange24h                     *decimal.Decimal            `json:"market_cap_change_24h"`
	MarketCapChangePercentage24h           *decimal.Decimal            `json:"market_cap_change_percentage_24h"`
	PriceChange24hInCurrency               map[string]*decimal.Decimal `json:"price_change_24h_in_currency"`
	PriceChangePercentage1hInCurrency      map[string]*decimal.Decimal `json:"price_change_percentage_1h_in_currency"`
	PriceChangePercentage24hInCurrency     map[string]*decimal.Decimal `json:"price_change_percentage_24h_in_currency"`
	PriceChangePercentage7dInCurrency      map[string]*decimal.Decimal `json:"price_change_percentage_7d_in_currency"`
	PriceChangePercentage14dInCurrency     map[string]*decimal.Decimal `json:"price_change_percentage_14d_in_currency"`
	PriceChangePercentage30dInCurrency     map[string]*decimal.Decimal `json:"price_change_percentage_30d_in_currency"`
	PriceChangePercentage60dInCurrency     map[string]*decimal.Decimal `json:"price_change_percentage_60d_in_currency"`
	PriceChangePercentage200dInCurrency    map[string]*decimal.Decimal `json:"price_change_percentage_200d_in_currency"`
	PriceChangePercentage1yInCurrency      map[string]*decimal.Decimal `json:"price_change_percentage_1y_in_currency"`
	MarketCapChange24hInCurrency           map[string]*decimal.Decimal `json:"market_cap_change_24h_in_currency"`
	MarketCapChangePercentage24hInCurrency map[string]*decimal.Decimal `json:"market_cap_change_percentage_24h_in_currency"`
	TotalSupply                            *decimal.Decimal            `json:"total_supply"`
	MaxSupply                              *decimal.Decimal            `json:"max_supply"`
	CirculatingSupply                      *decimal.Decimal            `json:"circulating_supply"`
	LastUpdated                            string                      `json:"last_updated"`
}

type CoinCommunityData struct {
	FacebookLikes            *decimal.Decimal `json:"facebook_likes"`
	TwitterFollowers         *decimal.Decimal `json:"twitter_followers"`
	RedditAveragePosts48h    *decimal.Decimal `json:"reddit_average_posts_48h"`
	RedditAverageComments48h *decimal.Decimal `json:"reddit_average_comments_48h"`
	RedditSubscribers        *decimal.Decimal `json:"reddit_subscribers"`
	RedditAccountsActive48h  *decimal.Decimal `json:"reddit_accounts_active_48h"`
	TelegramChannelUserCount *decimal.Decimal `json:"telegram_channel_user_count"`
}

type CoinDeveloperData struct {
	Forks                          *decimal.Decimal                    `json:"forks"`
	Stars                          *decimal.Decimal                    `json:"stars"`
	Subscribers                    *decimal.Decimal                    `json:"subscribers"`
	TotalIssues                    *decimal.Decimal                    `json:"total_issues"`
	ClosedIssues                   *decimal.Decimal                    `json:"closed_issues"`
	PullRequestsMerged             *decimal.Decimal                    `json:"pull_requests_merged"`
	PullRequestContributors        *decimal.Decimal                    `json:"pull_request_contributors"`
	CodeAdditionsDeletions4Weeks   CoinDeveloperDataDeletionsAdditions `json:"code_additions_deletions_4_weeks"`
	CommitCount4Weeks              *decimal.Decimal                    `json:"commit_count_4_weeks"`
	Last4WeeksCommitActivitySeries []*decimal.Decimal                  `json:"last_4_weeks_commit_activity_series"`
}

type CoinDeveloperDataDeletionsAdditions struct {
	Additions *decimal.Decimal `json:"additions"`
	Deletions *decimal.Decimal `json:"deletions"`
}

type CoinPublicInterestStats struct {
	AlexaRank   *decimal.Decimal `json:"alexa_rank"`
	BingMatches *decimal.Decimal `json:"bing_matches"`
}

type CoinStatusUpdate struct {
	Description string                  `json:"description"`
	Category    string                  `json:"category"`
	CreatedAt   string                  `json:"created_at"`
	User        string                  `json:"user"`
	UserTitle   string                  `json:"user_title"`
	Pin         bool                    `json:"pin"`
	Project     CoinStatusUpdateProject `json:"project"`
}

type CoinStatusUpdateProject struct {
	ID     string     `json:"id"`
	Symbol string     `json:"symbol"`
	Name   string     `json:"name"`
	Type   string     `json:"type"`
	Image  ImageLinks `json:"image"`
}

type CoinTicker struct {
	Base                   string                      `json:"base"`
	Target                 string                      `json:"target"`
	Market                 CoinTickerMarket            `json:"market"`
	Last                   *decimal.Decimal            `json:"last"`
	Volume                 *decimal.Decimal            `json:"volume"`
	ConvertedLast          map[string]*decimal.Decimal `json:"converted_last"`
	TrustScore             string                      `json:"trust_score"`
	BidAskSpreadPercentage *decimal.Decimal            `json:"bid_ask_spread_percentage"`
	Timestamp              string                      `json:"timestamp"`
	LastTradedAt           string                      `json:"last_traded_at"`
	LastFetchAt            string                      `json:"last_fetch_at"`
	IsAnomaly              bool                        `json:"is_anomaly"`
	IsStale                bool                        `json:"is_stale"`
	TradeURL               string                      `json:"trade_url"`
	TokenInfoURL           string                      `json:"token_info_url"`
	CoinID                 string                      `json:"coin_id"`
	TargetCoinID           string                      `json:"target_coin_id"`
}

type CoinTickerMarket struct {
	Name                string `json:"name"`
	Identifier          string `json:"identifier"`
	HasTradingIncentive bool   `json:"has_trading_incentive"`
}

type CoinMarket struct {
	ID                           string             `json:"id"`
	Symbol                       string             `json:"symbol"`
	Name                         string             `json:"name"`
	Image                        string             `json:"image"`
	CurrentPrice                 *decimal.Decimal   `json:"current_price"`
	MarketCap                    *decimal.Decimal   `json:"market_cap"`
	MarketCapRank                int                `json:"market_cap_rank"`
	FullyDilutedValuation        *decimal.Decimal   `json:"fully_diluted_valuation"`
	TotalVolume                  *decimal.Decimal   `json:"total_volume"`
	High24h                      *decimal.Decimal   `json:"high_24h"`
	Low24h                       *decimal.Decimal   `json:"low_24h"`
	PriceChange24h               *decimal.Decimal   `json:"price_change_24h"`
	PriceChangePercentage24h     *decimal.Decimal   `json:"price_change_percentage_24h"`
	MarketCapChange24h           *decimal.Decimal   `json:"market_cap_change_24h"`
	MarketCapChangePercentage24h *decimal.Decimal   `json:"market_cap_change_percentage_24h"`
	CirculatingSupply            *decimal.Decimal   `json:"circulating_supply"`
	TotalSupply                  *decimal.Decimal   `json:"total_supply"`
	MaxSupply                    *decimal.Decimal   `json:"max_supply"`
	ATH                          *decimal.Decimal   `json:"ath"`
	ATHChangePercentage          *decimal.Decimal   `json:"athChange_percentage"`
	ATHDate                      string             `json:"ath_date"`
	ATL                          *decimal.Decimal   `json:"atl"`
	ATLChangePercentage          *decimal.Decimal   `json:"atl_change_percentage"`
	ATLDate                      string             `json:"atl_date"`
	ROI                          ReturnOnInvestment `json:"roi"`
	LastUpdated                  string             `json:"last_updated"`
}

type ReturnOnInvestment struct {
	Times      *decimal.Decimal `json:"times"`
	Currency   string           `json:"currency"`
	Percentage *decimal.Decimal `json:"percentage"`
}

type CoinTickers struct {
	Name    string              `json:"name"`
	Tickers []CoinTickersTicker `json:"tickers"`
}

type CoinTickersTicker struct {
	Base                   string                           `json:"base"`
	Target                 string                           `json:"target"`
	Market                 CoinTickersTickerMarket          `json:"market"`
	Last                   *decimal.Decimal                 `json:"last"`
	Volume                 *decimal.Decimal                 `json:"volume"`
	ConvertedLast          CoinTickersTickerConvertedLast   `json:"converted_last"`
	ConvertedVolume        CoinTickersTickerConvertedVolume `json:"converted_volume"`
	TrustScore             string                           `json:"trust_score"`
	BidAskSpreadPercentage *decimal.Decimal                 `json:"bid_ask_spread_percentage"`
	Timestamp              string                           `json:"timestamp"`
	LastTradedAt           string                           `json:"last_traded_at"`
	LastFetchAt            string                           `json:"last_fetch_at"`
	IsAnomaly              bool                             `json:"is_anomaly"`
	IsStale                bool                             `json:"is_stale"`
	TradeURL               string                           `json:"trade_url"`
	TokenInfoURL           string                           `json:"token_info_url"`
	CoinID                 string                           `json:"coin_id"`
	TargetCoinID           string                           `json:"target_coin_id"`
}

type CoinTickersTickerMarket struct {
	Name                string `json:"name"`
	Identifier          string `json:"identifier"`
	HasTradingIncentive bool   `json:"has_trading_incentive"`
	Logo                string `json:"logo"`
}

type CoinTickersTickerConvertedLast struct {
	BTC *decimal.Decimal `json:"btc"`
	ETH *decimal.Decimal `json:"eth"`
	USD *decimal.Decimal `json:"usd"`
}

type CoinTickersTickerConvertedVolume struct {
	BTC *decimal.Decimal `json:"btc"`
	ETH *decimal.Decimal `json:"eth"`
	USD *decimal.Decimal `json:"usd"`
}

type CoinHistory struct {
	ID                  string                   `json:"id"`
	Symbol              string                   `json:"symbol"`
	Name                string                   `json:"name"`
	Localization        map[string]string        `json:"localization"`
	Image               ImageLinks               `json:"image"`
	MarketData          CoinHistoryMarketData    `json:"market_data"`
	CommunityData       CoinHistoryCommunityData `json:"community_data"`
	DeveloperData       CoinDeveloperData        `json:"developer_data"`
	PublicInterestStats CoinPublicInterestStats  `json:"public_interest_stats"`
}

type CoinHistoryMarketData struct {
	CurrentPrice map[string]*decimal.Decimal `json:"current_price"`
	MarketCap    map[string]*decimal.Decimal `json:"market_cap"`
	TotalVolume  map[string]*decimal.Decimal `json:"total_volume"`
}

type CoinHistoryCommunityData struct {
	FacebookLikes            *decimal.Decimal `json:"facebook_likes"`
	TwitterFollowers         *decimal.Decimal `json:"twitter_followers"`
	RedditAveragePosts48h    *decimal.Decimal `json:"reddit_average_posts_48h"`
	RedditAverageComments48h *decimal.Decimal `json:"reddit_average_comments_48h"`
	RedditSubscribers        *decimal.Decimal `json:"reddit_subscribers"`
	RedditAccountsActive48h  *decimal.Decimal `json:"reddit_accounts_active_48h"`
}

type CoinMarketChart struct {
	Prices       [][]*decimal.Decimal `json:"prices"`
	MarketCaps   [][]*decimal.Decimal `json:"market_caps"`
	TotalVolumes [][]*decimal.Decimal `json:"total_volumes"`
}

type CoinStatusUpdates struct {
	StatusUpdates []CoinStatusUpdate `json:"status_updates"`
}

type CoinOHLC [][]*decimal.Decimal

type CoinCategoryList struct {
	ID   string `json:"category_id"`
	Name string `json:"name"`
}
