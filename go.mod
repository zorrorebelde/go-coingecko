module gitlab.com/zorrorebelde/go-coingecko

go 1.20

require (
	github.com/hashicorp/go-retryablehttp v0.7.2
	github.com/insprac/qe v0.1.2
	github.com/shopspring/decimal v1.3.1
)

require github.com/hashicorp/go-cleanhttp v0.5.2
